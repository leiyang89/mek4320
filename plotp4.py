
from scitools.std import *
size = 260

x0 = []
y0 = []
fe = open('p6i.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x0.append(float(words[0]))
	y0.append(float(words[1]))
xp0 = array(x0[:size])
yp0 = array(y0[:size])
fe.close()
plot(xp0,yp0)

hold('on')

x1 = []
y1 = []
fe = open('p6ii.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x1.append(float(words[0]))
	y1.append(float(words[1]))
xp1 = array(x1[:size])
yp1 = array(y1[:size])
fe.close()
plot(xp1,yp1)
'''

x2 = []
y2 = []
fe = open('p4d_corr.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x2.append(float(words[0]))
	y2.append(float(words[1]))
xp2 = array(x2[:size])
yp2 = array(y2[:size])
fe.close()
plot(xp2,yp2)

x3 = []
y3 = []
fe = open('p4b_eta_after.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x3.append(float(words[0]))
	y3.append(float(words[1]))
xp3 = array(x3[:size])
yp3 = array(y3[:size])
fe.close()
plot(xp3,yp3)

x4 = []
y4 = []
fe = open('p4c_before.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x4.append(float(words[0]))
	y4.append(float(words[1]))
xp4 = array(x4[:size])
yp4 = array(y4[:size])
fe.close()
plot(xp4,yp4)

x5 = []
y5 = []
fe = open('p4c_after.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x5.append(float(words[0]))
	y5.append(float(words[1]))
xp5 = array(x5[:size])
yp5 = array(y5[:size])
fe.close()
plot(xp5,yp5)
'''
title('Prob 6')
legend('Bouss t=100, dx = 0.5','LSW t=100, dx = 2.06')
#'LSW t=0', 'LSW t=80',
savefig('prob6compare.png')
