
from scitools.std import *
size = 200
x0 = []
y0 = []
fe = open('x2eta_before.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x0.append(float(words[0]))
	y0.append(float(words[1]))
xp0 = array(x0[:size])
yp0 = array(y0[:size])
fe.close()
plot(xp0,yp0)
hold('on')

x1 = []
y1 = []
fe = open('x2eta_after.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x1.append(float(words[0]))
	y1.append(float(words[1]))
xp1 = array(x1[:size])
yp1 = array(y1[:size])
fe.close()
plot(xp1,yp1)

x2 = []
y2 = []
fe = open('x1eta_before.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x2.append(float(words[0]))
	y2.append(float(words[1]))
xp2 = array(x2[:2*size])
yp2 = array(y2[:2*size])
fe.close()
plot(xp2,yp2)

x3 = []
y3 = []
fe = open('x1eta_after.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x3.append(float(words[0]))
	y3.append(float(words[1]))
xp3 = array(x3[:2*size])
yp3 = array(y3[:2*size])
fe.close()
plot(xp3,yp3)

x4 = []
y4 = []
fe = open('x5eta_before.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x4.append(float(words[0]))
	y4.append(float(words[1]))
xp4 = array(x4[:4*size])
yp4 = array(y4[:4*size])
fe.close()
plot(xp4,yp4)

x5 = []
y5 = []
fe = open('x5eta_after.dat', 'r')
for line in fe:
	line.strip()
	words = line.split()
	x5.append(float(words[0]))
	y5.append(float(words[1]))
xp5 = array(x5[:4*size])
yp5 = array(y5[:4*size])
fe.close()
plot(xp5,yp5)
legend('t=0, dx =2', 't=70, dx=2', 't=0, dx = 1', 't=70, dx=1', 't=0, dx=0.5', 't=70, dx=0.5')

y1 = sorted(y1)
print y1[-1]
y3 = sorted(y3)
print y3[-1]
y5 = sorted(y5)
print y5[-1]
savefig('prob2.png')
