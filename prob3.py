from scitools.std import *

xsteps = 200

def wave(x):
	if (x > .5*lamb):
		return 0
	else:
		return (cos(pi*x/lamb))**2
	
x = linspace(0,100,xsteps)

