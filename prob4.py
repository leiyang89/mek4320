from scitools.std import *

xsteps = 130./(.5*sqrt(17))
lamb = 16

def wave(x):
	if (x > .5*lamb):
		return 0
	else:
		return .2*(cos(pi*x/lamb))**2
	
x = linspace(0,130,xsteps)
y = zeros(xsteps)
for i in range(len(x)):
	y[i] = wave(x[i])

f = open('eta.in','w')
for i in range(len(x)):
	out = str(x[i]) + ' ' + str(y[i]) + '\n'
	f.write(out)
f.close()		

